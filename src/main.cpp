#include <Arduino.h>

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(SERIAL_BAUD_RATE);
  delay(100);
  Serial.println("Hello world");
}

void loop()
{
  // put your main code here, to run repeatedly:
  Serial.println(millis());
  delay(1000);
}